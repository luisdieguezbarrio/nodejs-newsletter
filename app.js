

const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");

// const port = 3000;
const port = process.env.PORT || 3000; // eso significa AND OR
const app = express();

app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static("publico"));   // para que se vean las imagens locales y el CSS local

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/signup.html");
});

app.post("/", function (req, res) {
    var nombre = req.body.nombre;
    var apellido = req.body.apellido;
    var email = req.body.email;

    console.log(email, nombre, apellido);

    var data = {
        members: [
            {
                email_address: email,
                status: "subscribed",
                merge_fields: {
                    FNAME: nombre,
                    LNAME: apellido
                }
            }
        ]
    };

    var dataJson = JSON.stringify(data);

    var options = {
        url: "https://us4.api.mailchimp.com/3.0/lists/150904d545",
        method: "POST",
        headers: {
            "Authorization": "xxxxx 1234-us4"
        },
        // body: dataJson
    };

    request(options, function (error, response, body) {
        // if (error) {
        //     console.log(error);
        // } else {
        //     console.log(response.statusCode);
        // };
        if (response.statusCode === 200 ) {
            console.log("Todo bien y todo correcto : " + response.statusCode);
            res.sendFile(__dirname + "/signupCorrecto.html");
        } else {
            console.log("Error " + error + response.statusCode);
            res.sendFile(__dirname + "/signupError.html");
        };
    });
});

app.post("/falloRegistro", function (req, res) {
    res.redirect("/");
})

app.listen(port, function () {
    console.log("Todo esta funcionando en el puerto " + port);
});
